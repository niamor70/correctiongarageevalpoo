<?php

class Voiture {
    public $id;
    public $id_modele;
    public $couleur;
    public $num_immat;
    public $louable;

    public function __construct($couleur_constr,$id_modele_constr,$num_immat_constr,$louable_constr)
    {
        $this->couleur = $couleur_constr;
        $this->id_modele = $id_modele_constr;
        $this->num_immat = $num_immat_constr;
        $this->louable = $louable_constr;
    }
}