<?php

class Modele {
    public $id;
    public $type;
    public $constructeur;
    public $nom;
    public $chevaux;
    public $combustible;
    public $volume_coffre;
    public $nb_place;
    public $nb_porte;
    public $boite_vitesse;

    public function __construct($type_constr,$constructeur_constr,$nom_constr,$chevaux_constr,$combustible_constr,$volume_coffre_constr,
    $nb_place_constr,$nb_porte_constr,$boite_vitesse_constr)
    {
        $this->type = $type_constr;
        $this->constructeur = $constructeur_constr;
        $this->nom = $nom_constr;
        $this->chevaux = $chevaux_constr;
        $this->combustible = $combustible_constr;
        $this->volume_coffre = $volume_coffre_constr;
        $this->nb_place = $nb_place_constr;
        $this->nb_porte = $nb_porte_constr;
        $this->boite_vitesse = $boite_vitesse_constr;
    }
}