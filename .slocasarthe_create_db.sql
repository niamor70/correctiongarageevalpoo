USE `cc_locasarthe`;

CREATE TABLE client(
    id_client              INT         AUTO_INCREMENT,
    nom             VARCHAR(255)     NOT NULL,
    prenom          VARCHAR(255)     NOT NULL,
    adresse         VARCHAR(255)     NOT NULL,
    code_postal     CHAR(5)     NOT NULL,
    ville           VARCHAR(255)     NOT NULL,
    telephone       CHAR(14),
    email           VARCHAR(255),
    date_naissance  DATE        NOT NULL,
    numero_permis   VARCHAR(12) NOT NULL,
    date_permis     DATE        NOT NULL,
    PRIMARY KEY (id_client),
    CHECK ( date_permis > date_naissance )
);

CREATE TABLE types(
    id_types      TINYINT     AUTO_INCREMENT,
    libelle VARCHAR(255)     NOT NULL,
    PRIMARY KEY (id_types)
);

CREATE TABLE modele(
    id_modele              MEDIUMINT   AUTO_INCREMENT,
    id_types         TINYINT     NOT NULL,
    constructeur    VARCHAR(255)     NOT NULL,
    nom             VARCHAR(255)     NOT NULL,
    chevaux         INT,
    combustible     VARCHAR(255),
    consommation    NUMERIC(4,2),
    volume_coffre   SMALLINT,
    nb_places       TINYINT,
    nb_portes       TINYINT,
    boite_vitesse   VARCHAR(255),
    PRIMARY KEY (id_modele),
    FOREIGN KEY (id_types) REFERENCES types(id_types),
    CHECK ( nb_places < 10 ),
    CHECK ( combustible IN ('diesel', 'essence', 'électrique', 'hybride') ),
    CHECK ( boite_vitesse IN ('automatique', 'manuelle', 'séquentielle') )
);

CREATE TABLE voiture(
    id_voiture                  INT         AUTO_INCREMENT,
    id_modele           MEDIUMINT   NOT NULL,
    couleur             VARCHAR(255)     NOT NULL,
    immatriculation     CHAR(9)     NOT NULL,
    louable             TINYINT     DEFAULT 1,
    PRIMARY KEY (id_voiture),
    FOREIGN KEY (id_modele) REFERENCES modele(id_modele),
    check ( louable IN (0, 1) )
);

CREATE TABLE locations(
    numero_resa     BIGINT      AUTO_INCREMENT,
    id_client       INT         NOT NULL,
    id_voiture      INT         NOT NULL,
    date_resa       DATE        NOT NULL,
    date_depart     DATETIME    NOT NULL,
    date_retour     DATETIME    NOT NULL,
    PRIMARY KEY (numero_resa),
    FOREIGN KEY (id_client) REFERENCES client(id_client),
    FOREIGN KEY (id_voiture) REFERENCES voiture(id_voiture),
    CHECK ( date_retour > date_depart )
);
